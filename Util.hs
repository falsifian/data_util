module Util
( fromNothingErr
, getSHA256FromHashList
, getUserName
, lookupAtomList
, randomID
) where

import qualified CAS.Hash
import qualified Codec.Binary.DataEncoding as DE
import Control.Monad
import Control.Monad.Error
import qualified Data.Map as Map
import Data.Map (Map)
import System.Exit
import System.IO
import System.Process
import System.Random
import Text.Printf
import Text.SExpression as SE
import Text.SExpression.Format
  
fromNothingErr :: String -> Maybe a -> IO a
fromNothingErr msg Nothing = hPutStrLn stderr (printf "missing value: %s" msg) >> exitWith (ExitFailure (-1))
fromNothingErr _msg (Just x) = return x

getSHA256FromHashList :: (Error e, MonadError e m) =>
                         [String] -> m CAS.Hash.SHA256
getSHA256FromHashList hashes =
  let hashPrefix = "sha256:" in
  case
    do (hashPrefix', hashData) <- liftM (splitAt (length hashPrefix)) hashes
       guard $ hashPrefix == hashPrefix'
       return hashData
  of [hashData] ->
       if 64 == length hashData
         then CAS.Hash.hexToSHA256 hashData
         else maybe
           (throwError $ strMsg "couldn't decode base64 hash")
           CAS.Hash.bytesToSHA256
           (DE.decode DE.base64 hashData)
     [] -> throwError $ strMsg "No sha256 hashes found."
     _ : _ : _ -> throwError $ strMsg "More than one sha256 hash found."

getUserName :: IO String
getUserName =     
  do (ExitSuccess, userNameNL, "") <- readProcessWithExitCode "whoami" [] ""
     let (userName, "\n") = splitAt (length userNameNL - 1) userNameNL
     return userName

lookupAtomList :: (Error e, MonadError e m) =>
                  String -> Map.Map String [Value] -> m (Maybe [String])
lookupAtomList key attribs =
  maybe
    (return Nothing)
    (\ses -> liftM Just $ sequence
       [ case el of
            SE.Atom s -> return s
            _ -> throwError $ strMsg $
                 printf "expected a hash but didn't get an atom: %s" (show el)
       | el <- ses
       ]
    )
    (Map.lookup key attribs)

-- TODO:  Maybe use a cryptographically strong prng.
randomID :: IO String
randomID =
  let alpha = ['0' .. '9'] ++ ['A' .. 'V'] in
  sequence $ replicate 14 $ liftM (alpha !!) $ randomRIO (0, length alpha - 1)
