module Main (main) where

import Command
--import Commands.AddBankStmt
import Commands.AddPhoto
import Commands.GC
import Commands.MakePhotoLinks
import Data.Maybe
import System.Environment
import System.Exit
import System.IO

commands :: [(String, Command)]
commands =
  --[ ("abs", Commands.AddBankStmt.command)
  [ ("aph", Commands.AddPhoto.command)
  , ("gc",  Commands.GC.command)
  -- Commands.MakePhotoLinks is broken.
  , ("mpl", Commands.MakePhotoLinks.command)
  ]

main :: IO ()
main = getArgs >>= \args ->
  case args of
    [] -> showUsage Nothing >> usageExit
    cmdName : _ ->
      case lookup cmdName commands of
        Nothing -> showUsage (Just ("unknown command: " ++ show cmdName)) >> usageExit
        Just cmd -> action cmd args

showUsage :: Maybe String -> IO ()
showUsage msg = hPutStr stderr $ unlines $ maybeToList msg ++ [usage cmd name | (name, cmd) <- commands]

usageExit :: IO a
usageExit = exitWith (ExitFailure (-1))
