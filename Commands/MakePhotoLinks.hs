-- Commands.MakePhotoLinks -- make a directory of symlinks to photos

module Commands.MakePhotoLinks (command) where

import qualified CAS.Hash
import Command
import Control.Monad
import Control.Monad.Error.Class
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.Format
import qualified Graphics.Exif as Exif
import Photo
import System.Console.GetOpt
import System.Directory
import System.FilePath
import System.IO
import System.IO.Error (catchIOError)
import System.Posix.Files
import Text.Printf
import Text.SExpression as SE
import Text.SExpression.Format
import Util
  
command :: Command
command = Cmd { action = getOptCmd defaultOptions optionDescrs main
              , usage = standardUsage optionDescrs Nothing
              }

main :: Options -> String -> [String] -> IO ()
main options cmdName args =
  do unless (null args) $ 
       do hPutStr stderr
            ( standardUsage
              optionDescrs
              (Just "Too many arguments.")
              cmdName
            )
     cwd <- getCurrentDirectory
     casPath <- fromNothingErr (standardUsage optionDescrs (Just "CAS path") cmdName) (optCasPath options)
     recPath <- fromNothingErr (standardUsage optionDescrs (Just "rec path") cmdName) (optRecPath options)
     symlinkPath <- fromNothingErr (standardUsage optionDescrs (Just "symlink path") cmdName) (optSymlinkPath options)
     doesDirectoryExist symlinkPath >>= (flip when $ throwError $ strMsg $ printf "Directory %s already exists." symlinkPath)
     -- Note: There is a race condition when two processes are trying to create
     -- exclusive directories; we can only hope that the system's
     -- implementation of createDirectory fails when the directory already
     -- exists.
     putStrLn $ printf "Creating a symlink directory at %s." symlinkPath
     createDirectory symlinkPath
     photos <- getAllPhotos recPath
     sequence_
       [ do photoSHA256 <- getPhotoSHA256 photoInfo
            let photoDataAbsolutePath = -- absolute to avoid confusion with symlink
                  cwd </> casPath </> "data" </> CAS.Hash.sha256ToHex photoSHA256  -- TODO: the code to get this path should really be in CAS.
            dateDir <- if optDateInPath options
                       then
                         do date <- getPhotoDate photoDataAbsolutePath
                            let p =
                                  case date of
                                    Nothing -> symlinkPath </> "no_date"
                                    Just date -> symlinkPath </> dateToPath date
                            createDirectoryIfMissing True p 
                            return p
                       else
                         return symlinkPath
            createSymbolicLink photoDataAbsolutePath (dateDir </> (photoID ++ "-" ++ recordID))
         | (photoID, recordID, photoInfo) <- photos
         ]

data Options =
  Options
  { optCasPath :: !(Maybe FilePath)
  , optDateInPath :: !Bool
  , optRecPath :: !(Maybe FilePath)
  , optSymlinkPath :: !(Maybe FilePath)
  }

optionDescrs :: [OptDescr (Options -> Options)]
optionDescrs =
  [ Option "c" []
    (ReqArg (\s o -> o {optCasPath = Just s}) "CAS path")
    "CAS path"
  , Option "D" []
    (NoArg (\o -> o {optDateInPath = True}))
    "include the date in the path"
  , Option "d" []
    (NoArg (\o -> o {optDateInPath = False}))
    "do not include the date in the path"
  , Option "r" []
    (ReqArg (\s o -> o {optRecPath = Just s}) "rec path")
    "rec path"
  , Option "s" []
    (ReqArg (\s o -> o {optSymlinkPath = Just s}) "symlink dir path")
    "symlink dir path"
  ]
  
defaultOptions :: Options
defaultOptions =
  Options
  { optDateInPath = True
  , optCasPath = Nothing
  , optRecPath = Nothing
  , optSymlinkPath = Just "photo-symlinks"
  }

getPhotoDate :: FilePath -> IO (Maybe UTCTime)
getPhotoDate path =
  do exif <- Exif.fromFile path
     mDateStr <- Exif.getTag exif "DateTimeOriginal"  -- TODO: handle missing DateTimeOriginal by trying other fields
     case mDateStr of
       Nothing -> return Nothing
       Just dateStr -> return (parseTimeM False defaultTimeLocale "%Y:%m:%d %H:%M:%S" dateStr)
  `catchIOError` const (return Nothing)

dateToPath :: UTCTime -> FilePath
dateToPath utct =
  let (year, month, dayOfMonth) = toGregorian (utctDay utct)
  in printf "%04d" year </> printf "%02d" month </> printf "%02d" dayOfMonth
