-- Commands.GC -- garbage collection

module Commands.GC (command) where

import qualified CAS
import qualified CAS.Hash
import Command
import Control.Monad
import qualified Data.Set as Set
import ImportRecord
import Photo
import System.Console.GetOpt
import System.FilePath
import Text.Printf
import Util

command :: Command
command = Cmd { action = getOptCmd defaultOptions optionDescrs main
              , usage = standardUsage optionDescrs Nothing
              }

main :: Options -> String -> [String] -> IO ()
main options cmdName [] =
  do casPath <- fromNothingErr (standardUsage optionDescrs (Just "CAS path") cmdName) (optCasPath options)
     recPath <- fromNothingErr (standardUsage optionDescrs (Just "rec path") cmdName) (optRecPath options)
     photoSHA256s <- getAllPhotos recPath
         >>= mapM (\(_, _, photoInfo) -> getPhotoSHA256 photoInfo)
     medSHA256s <- getAllImportRecords (recPath </> "med")
         >>= mapM (\(_, importRecord) -> getImportRecordSHA256 importRecord)
     let allExpectedHashes = Set.fromList (photoSHA256s ++ medSHA256s)
     allHashes <- liftM Set.fromList $ CAS.run CAS.listAll casPath
     let missing = Set.difference allExpectedHashes allHashes
     unless (Set.null missing) $
       do printf "Warning: missing data for %d hashes:\n" (Set.size missing)
          putStrLn $ unlines $ map CAS.Hash.sha256ToHex $ Set.toList missing
     let extra = Set.difference allHashes allExpectedHashes
     printf "%d extra hashes would be garbage-collected%s\n" (Set.size extra) (if Set.null extra then "" else ":")
     putStrLn $ unlines $ map CAS.Hash.sha256ToHex $ Set.toList extra

data Options =
  Options
  { optCasPath :: !(Maybe FilePath)
  , optRecPath :: !(Maybe FilePath)
  }

optionDescrs :: [OptDescr (Options -> Options)]
optionDescrs =
  [ Option "c" []
    (ReqArg (\s o -> o {optCasPath = Just s}) "CAS path")
    "CAS path"
  , Option "r" []
    (ReqArg (\s o -> o {optRecPath = Just s}) "rec path")
    "rec path"
  ]

defaultOptions :: Options
defaultOptions =
  Options
  { optCasPath = Nothing
  , optRecPath = Nothing
  }
